﻿using Microsoft.Azure.Devices.Client;
using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Gpio;
using Windows.Media.Capture;
using Windows.Media.MediaProperties;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Controls;

namespace Alarm
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private GpioPin redLed, greenLed, sensor;
        private DeviceClient deviceClient;
        private MediaCapture mediaCapture;
        private const string LEVEL_PROPERTY = "level";
        private bool stop = true;

        public MainPage()
        {
            this.InitializeComponent();
            InitSensor();
            InitDeviceClient();
            InitVideo();
            ReceiveMessage();
            SendStatusMessageRegularly();
        }

        private void InitSensor()
        {
            var gpio = GpioController.GetDefault();
            sensor = gpio.OpenPin(21);
            redLed = gpio.OpenPin(3);
            greenLed = gpio.OpenPin(4);
            sensor.SetDriveMode(GpioPinDriveMode.Input);
            redLed.SetDriveMode(GpioPinDriveMode.Output);
            greenLed.SetDriveMode(GpioPinDriveMode.Output);
            Task.Delay(3000);
            BlinkingGreenLed();
            sensor.ValueChanged += Sensor_ValueChanged;           
        }

        private void Sensor_ValueChanged(GpioPin sender, GpioPinValueChangedEventArgs args)
        {
            if (sender.Read() == GpioPinValue.High)
            {
                redLed.Write(GpioPinValue.Low);
                TakePhoto();
            }
            else
            {
                redLed.Write(GpioPinValue.High);
            }
        }

        private void InitDeviceClient()
        {
            //in app.config change version of System.Net.Http on 4.0.0.0

            const string HOST_NAME = "", // <IoTHubName>.azure-devices.net
                         DEVICE_ID = "alarm", 
                         DEVICE_KEY = "";

            deviceClient = DeviceClient.Create(HOST_NAME, AuthenticationMethodFactory.CreateAuthenticationWithRegistrySymmetricKey(DEVICE_ID, DEVICE_KEY), TransportType.Http1);

            Debug.WriteLine("DeviceClient is initialized");
        }

        private async void InitVideo()
        {
            mediaCapture = new MediaCapture();
            await mediaCapture.InitializeAsync();
            Debug.WriteLine("Media capture is initialized");
            previewVideo.Source = mediaCapture;
            Debug.WriteLine("Preview video is set");
            await mediaCapture.StartPreviewAsync();
            Debug.WriteLine("Video preview is started");
        }

        private async void TakePhoto()
        {
            ImageEncodingProperties imageProperties = ImageEncodingProperties.CreateJpeg();
            imageProperties.Height = 400;
            imageProperties.Width = 400;
            
            using (var photo = new InMemoryRandomAccessStream())
            {
                await mediaCapture.CapturePhotoToStreamAsync(imageProperties, photo);
                Debug.WriteLine("Photo is taken");

                if (!stop)
                {
                    var reader = new DataReader(photo.GetInputStreamAt(0));
                    var bytes = new byte[photo.Size];
                    await reader.LoadAsync((uint)photo.Size);
                    reader.ReadBytes(bytes);
                    await SendAlarmMessage(bytes);
                }
                
                stop = true;
            }
        }
        
        private async Task SendAlarmMessage(byte[] photo)
        {
            var message = new Message(Encoding.ASCII.GetBytes($"Alarm {DateTime.Now}"));
            message.ContentType = "text";
            message.Properties.Add(LEVEL_PROPERTY, "critical");
            await deviceClient.SendEventAsync(message);

            Debug.WriteLine("Start sending photo");
            message = new Message(photo);
            message.Properties.Add(LEVEL_PROPERTY, "critical");
            message.ContentType = "photo";
            await deviceClient.SendEventAsync(message);

            Debug.WriteLine("Photo is sent");
        }

        private async void ReceiveMessage()
        {
            var delay = new TimeSpan(0, 0, 10); //10s
            while (true)
            {
                var message = await deviceClient.ReceiveAsync();
                if (message == null)
                    continue;

                var msgStr = Encoding.ASCII.GetString(message.GetBytes());
                await deviceClient.CompleteAsync(message);
                if (msgStr == "status")
                {
                    await SendStatusMessage();
                }
                else if (msgStr == "start")
                {
                    stop = false;
                }else if(msgStr == "stop")
                {
                    stop = true;
                }

                await Task.Delay(delay);
            }
        }

        private async Task SendStatusMessage()
        {
            var message = new Message(Encoding.ASCII.GetBytes($"I am working :) {DateTime.Now}"));
            message.ContentType = "text";
            message.Properties.Add(LEVEL_PROPERTY, "low");
            await deviceClient.SendEventAsync(message);
        }

        private async void SendStatusMessageRegularly()
        {
            var delay = new TimeSpan(0, 5, 0); //5 min

            while (true)
            {
                await SendStatusMessage();
                await Task.Delay(delay);
            }
        }

        private async void BlinkingGreenLed()
        {
            while (true)
            {
                if (stop) //blinking
                {
                    greenLed.Write(greenLed.Read() == GpioPinValue.Low ? GpioPinValue.High : GpioPinValue.Low);
                }
                else
                {
                    greenLed.Write(GpioPinValue.Low);
                }
                await Task.Delay(500);
            }
        }

    }
}
