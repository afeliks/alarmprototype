﻿using MessageManagement.MySql;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using System;
using System.IO;
using System.Text;

namespace MessageManagement
{
    public class Functions
    {
        //connection string for storage
        //you can find it in Storage -> Access keys -> Conncection String
        private const string LOCAL_STORAGE_CONNECTION_STRING = ""; 
        //for local sotrage //@"UseDevelopmentStorage=true;";
        private const string CONTENT_TYPE_PROP = "content-type";
        private const string CONTENT_ENCODING_PROP = "content-encoding";
        private const string CONTENT_TYPE_TEXT = "text";
        private const string CONTENT_TYPE_PHOTO = "photo";

        private static Mail.IMail Email = new Mail.Mail();
        private static Storage.IStorage Storage = new Storage.Storage(LOCAL_STORAGE_CONNECTION_STRING);
        private static MySqlDB DB = new MySqlDB();

        //https://github.com/Azure/azure-webjobs-sdk/wiki/EventHub-support
        //This method is executed when somme messages (events) are in eventhub
        public void GetStatus([EventHubTrigger(Program.EVENT_HUB_NAME)] EventData message)
        {
            string contentType = message.SystemProperties[CONTENT_TYPE_PROP] as string;

            var messageContentBytes = message.GetBytes();

            if (contentType == CONTENT_TYPE_PHOTO)
            {
                Storage.SendFileFromBytes(messageContentBytes, $"{DateTime.Now.Ticks}.jpg");

                ////File.WriteAllBytes($@"photos\logo{DateTime.Now.Ticks}.jpg", messageContentBytes); //for local
            }
            else if(contentType == CONTENT_TYPE_TEXT)
            {
                var contentText = Encoding.ASCII.GetString(messageContentBytes);
                Console.WriteLine(contentText);
                DB.Insert($"Status - {contentText}", DateTime.Now);
            }
        }

        //https://docs.microsoft.com/en-us/azure/azure-functions/functions-dotnet-class-library
        //https://social.technet.microsoft.com/wiki/contents/articles/31981.azure-webjobs-servicebustrigger.aspx
        //This method is executed when somme messages are in queue
        public void GetAlarm([ServiceBusTrigger("criticalalarmsqueue")] BrokeredMessage message)
        {
            Stream stream = message.GetBody<Stream>();

            if (message.ContentType == CONTENT_TYPE_TEXT)
            {                
                StreamReader reader = new StreamReader(stream, Encoding.ASCII);
                string text = reader.ReadToEnd();
                Console.WriteLine(text);
                Email.Send("Alarm!", text, System.Net.Mail.MailPriority.High);
                DB.Insert($"Alarm! - {text}", DateTime.Now);
            }
            else if(message.ContentType == CONTENT_TYPE_PHOTO && stream != null) 
            {                
                byte[] photo = new byte[stream.Length];
                stream.Read(photo, 0, photo.Length);
                var fileName = $"{Path.GetTempPath()}{DateTime.Now.Ticks}.jpg";
                
                File.WriteAllBytes(fileName, photo);
                var fs = File.Open(fileName, FileMode.Open);

                Storage.SaveFileFromStream(fs, $"{DateTime.Now.Ticks}.jpg");                
                //For local use
                //byte[] photo = new byte[stream.Length];
                //stream.Read(photo, 0, photo.Length);
                //File.WriteAllBytes($@"photos\{DateTime.Now.Ticks}.jpg", photo);
            }

            message.Complete();
        }
    }
}
