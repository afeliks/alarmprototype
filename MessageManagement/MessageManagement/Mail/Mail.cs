﻿using System.Diagnostics;
using System.Net;
using System.Net.Mail;

namespace MessageManagement.Mail
{
    public class Mail : IMail
    {
        private SmtpClient Client { get; }
        private const string PASSWORD = "";
        private const string LOGIN = "@onet.pl";

        public Mail()
        {
            Client = new SmtpClient();
            Client.Port = 587; //SSL is not needed
            Client.Host = "smtp.poczta.onet.pl";
            Client.Credentials = new NetworkCredential(LOGIN, PASSWORD);
        }

        public void Send(string subject, string message, MailPriority mailPriority = MailPriority.Normal)
        {
            MailMessage mail = new MailMessage(LOGIN, LOGIN);
            mail.Body = message;
            mail.Subject = subject;
            mail.Priority = mailPriority;
            try
            {
                Client.Send(mail);
            }
            catch (SmtpException ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }
    }
}
