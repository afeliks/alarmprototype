﻿using System.Net.Mail;

namespace MessageManagement.Mail
{
    interface IMail
    {
        void Send(string subject, string message, MailPriority mailPriority = MailPriority.Normal);
    }
}
