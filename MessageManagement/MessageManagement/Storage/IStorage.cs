﻿using System.IO;

namespace MessageManagement.Storage
{
    interface IStorage
    {
        void SaveFileFromStream(Stream file, string fileName);
        void SendFileFromBytes(byte[] file, string fileName);
    }
}
