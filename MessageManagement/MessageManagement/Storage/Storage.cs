﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;

namespace MessageManagement.Storage
{
    public class Storage : IStorage
    {
        private CloudStorageAccount storageAccount;
        private CloudBlobClient blobClient;
        private CloudBlobContainer blobContainer;

        public Storage(string connectionString)
        {
            storageAccount = CloudStorageAccount.Parse(connectionString);
            blobClient = storageAccount.CreateCloudBlobClient();
            CreateContainerIfNotExists("alarm");
        }

        private void CreateContainerIfNotExists(string containerName)
        {
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);

            container.CreateIfNotExists();
            blobContainer = container;            
        }

        public void SaveFileFromStream(Stream file, string fileName)
        {
            CloudBlockBlob blob = GetBlobReference(fileName);
            blob.UploadFromStream(file);
        }

        public void SendFileFromBytes(byte[] file, string fileName)
        {
            CloudBlockBlob blob = GetBlobReference(fileName);
            blob.UploadFromByteArray(file, 0, file.Length);
        }

        private CloudBlockBlob GetBlobReference(string fileName)
        {
            return blobContainer.GetBlockBlobReference(fileName);
        }
    }
}
