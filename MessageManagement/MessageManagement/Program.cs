﻿using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.ServiceBus;

namespace MessageManagement
{
    public class Program
    {
        /* 
         * Be carefull event hub is not the same as IoT Hub. Iot Hub uses Event Hub
         * Here we try connect to event hub not IoT Hub
         */ 

        //The below information you can find in IotHub -> Endpoints -> Events
        public const string EVENT_HUB_NAME = "";
        //The below information you can find in IotHub -> Endpoints -> Events
        private const string RECEIVER_CONNNECTION_STRING = "";
        //The below information you can find in ServiceBus -> Shared access policies -> RootManageSharedAccessKey
        private const string SERVICE_BUS_CONNECTION_STRING = "";

        static void Main()
        {
            var config = new JobHostConfiguration();

            var eventHubConf = new EventHubConfiguration();
            eventHubConf.AddReceiver(EVENT_HUB_NAME, RECEIVER_CONNNECTION_STRING);

            var serviceBusConf = new ServiceBusConfiguration();
            serviceBusConf.ConnectionString = SERVICE_BUS_CONNECTION_STRING;
          
            config.UseEventHub(eventHubConf);
            config.UseServiceBus(serviceBusConf);
            var host = new JobHost(config);
            //The following code ensures that the WebJob will be running continuously
            host.RunAndBlock();
        }
    }
}
