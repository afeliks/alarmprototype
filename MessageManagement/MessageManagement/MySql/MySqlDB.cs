﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace MessageManagement.MySql
{
    public class MySqlDB
    {
        private MySqlConnection Conn { get; }

        public MySqlDB()
        {
            //Uid and pwd you can find on FTP /data/mysql -> MYSQLCONNSTR_localdb.ini
            //WebApp -> Get Publish Profile. There you can find ftp address.
            Conn = new MySqlConnection("Server=127.0.0.1;Port=54085;Database=localdb;Uid=;Pwd=;");
            Conn.Open();
        }

        public void Insert(string status, DateTime date)
        {
            var cmd = new MySqlCommand($"Insert into status (status, date) values ('{status}','{ date.ToString("yyyy-MM-dd HH:mm")}')", Conn);
            cmd.ExecuteNonQuery();
        }
    }
}
