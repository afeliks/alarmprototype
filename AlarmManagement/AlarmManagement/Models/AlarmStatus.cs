﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlarmManagement.Models
{
    public class AlarmStatus
    {
        public string Status { get; private set; }
        public DateTime Date { get; private set; }

        public AlarmStatus(string status, DateTime date)
        {
            Status = status;
            Date = date;
        }
    }
}
