﻿using AlarmManagement.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Devices;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace AlarmManagement.Controllers
{
    public class HomeController : Controller
    {
        //Connection string to IoT Hub. 
        //You can find it in Iot Hub -> Shared access policies -> iothubowner
        private const string SERVICE_CLIENT_CONNECTION_STRING = "";
        private const string DEVICE_ID = "alarm";

        public IActionResult Index()
        {
            ViewData["statuses"] = GetStauses(); 
            return View();
        }

        private List<AlarmStatus> GetStauses()
        {
            var list = new List<AlarmStatus>();

            MySqlConnection conn = new MySqlConnection("Server=127.0.0.1;Port=54085;Database=localdb;Uid=azure;Pwd=6#vWHD_$;");            
            conn.Open();
            MySqlCommand cmd = new MySqlCommand("SELECT status, date FROM status", conn);
            var rdr = cmd.ExecuteReader();
            while(rdr.Read())
            {
                list.Add(new AlarmStatus(rdr[0].ToString(), DateTime.Parse(rdr[1].ToString())));
            }

            conn.Close();

            return list.OrderByDescending(l => l.Date).ToList();
        }

        public IActionResult GetStatus()
        {
            SendStatusMessageToDevice();
            Thread.Sleep(5000);
            return Redirect("Index");
        }

        public IActionResult StartAlarm()
        {
            SendStartMessageToDevice();
            return Redirect("Index");
        }

        public IActionResult StopAlarm()
        {
            SendStopMessageToDevice();
            return Redirect("Index");
        }

        //https://docs.microsoft.com/en-us/azure/iot-hub/iot-hub-csharp-csharp-c2d
        private void SendStatusMessageToDevice()
        {
            ServiceClient serviceClient = ServiceClient.CreateFromConnectionString(SERVICE_CLIENT_CONNECTION_STRING);
            var message = new Message(Encoding.ASCII.GetBytes("status"));
            serviceClient.SendAsync(DEVICE_ID, message);
        }

        private void SendStartMessageToDevice()
        {
            ServiceClient serviceClient = ServiceClient.CreateFromConnectionString(SERVICE_CLIENT_CONNECTION_STRING);
            var message = new Message(Encoding.ASCII.GetBytes("start"));
            serviceClient.SendAsync(DEVICE_ID, message);
        }

        private void SendStopMessageToDevice()
        {
            ServiceClient serviceClient = ServiceClient.CreateFromConnectionString(SERVICE_CLIENT_CONNECTION_STRING);
            var message = new Message(Encoding.ASCII.GetBytes("stop"));
            serviceClient.SendAsync(DEVICE_ID, message);
        }
    }
}
